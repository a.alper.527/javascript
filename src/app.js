let sayi1 = 10
sayi1  = "Engin Demiroğ"
let student = {id:1, name:"Engin"}
//console.log(student);

function save(puan=10,ogrenci) {
    console.log(ogrenci.name + " " + puan)
}

//save(undefined,student)

let students = ["Alperen Dağıstan", "Ersin Öztürk", "Levent Farse", "Kubi"]

//console.log(students)

let students2 = [student,{id:2, name:"Ersin"},"Ankara",{city:"İstanbul"}]

//console.log(students2)

//rest
//params
//varArgs
let showProducts = function (id,...products) {
    console.log(id)
    console.log(products)
    
}

//console.log(typeof showProducts)

//showProducts(10,["Elma", "Armut","Karpuz"])

//spread = ayrıştırmak
let points = [1,2,3,4,5,10,6]
console.log(...points)
console.log(Math.max(...points))

//Destructuring, dizideki her bir elemanı bir değişkene atamak

let populations = [10000,20000,30000,[40000,100000]]
let [small,medium,high,[veryHigh,maximum]] = populations
console.log(small)
console.log(medium)
console.log(high)
console.log(veryHigh)
console.log(maximum)

function someFunctions([small1,medium1],number) {
    console.log(small1,medium1)
    
}
someFunctions(populations)

let category = {id:1,name:"İçecek"}

console.log(category.id)
console.log(category["name"])

let {id,name} = category

console.log(id)
console.log(name)

//Redux mimarisinde üstteki 3 durumu çok sık kullanacağız


